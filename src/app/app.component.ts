import { Component, ViewChild } from '@angular/core';
import { BreakpointObserver, Breakpoints } from '@angular/cdk/layout';
import { Observable } from 'rxjs';
import { map, shareReplay } from 'rxjs/operators';
import { MatSidenav } from '@angular/material';
import { NavigationService } from './shared/service/navigation.service';


export interface TableElement {
    id: string;
    name: string;
    email: string;
    website: string;
}

@Component({
    selector: 'app-root',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent {

    @ViewChild('drawer') drawer: MatSidenav;

    isHandset$: Observable<boolean> = this.breakpointObserver
        .observe(Breakpoints.Handset)
        .pipe(
            map(result => result.matches),
            shareReplay()
        );


    constructor(
        private breakpointObserver: BreakpointObserver,
        private navigationService: NavigationService) {
    }

    onRouterClick(route: string) {
        if (this.drawer.mode === 'over') {
            this.drawer.toggle();
        }
        this.navigationService.navigateByUrl('/' + route);
    }

    onBackButtonClicked() {
        this.navigationService.back();
    }
}
