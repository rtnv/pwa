import { HttpClient } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';

@Component({
   selector: 'app-home',
   templateUrl: './home.component.html',
   styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

   jokeStr = 'Welcome!';
   jokeUrl = 'https://icanhazdadjoke.com/slack';

   constructor(private http: HttpClient) { }

   ngOnInit(): void {
      this.next();
   }

   next() {
      this.http.get(this.jokeUrl).toPromise().then(
         (result: any) => {
            console.log(JSON.stringify(result));
            this.jokeStr = result.attachments[0].text.toString();
         }
      );
   }

}
